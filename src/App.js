import React, { Component } from 'react';
import './App.css';

import Header from './components/Header';
import Body from './components/Body';
import Footer from './components/Footer';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fromCompany: "Your Comapny Name",
    };
  }
  AppHandleChange = (name, value) => {
    console.log(16, name, value);
    
    this.setState({
      [name]: value,
    });
  };
  printPage = () => {
    window.print();
  }
  render() {
    return (
      <div className="App">
        <div className="text-right hide-on-print"><button onClick={this.printPage}>Print</button></div>
        <Header AppHandleChange={this.AppHandleChange} fromCompany={this.state.fromCompany} />
        <Body />
        <Footer fromCompany={this.state.fromCompany} />
      </div>
    );
  }
}

export default App;
