import React, { Component } from 'react';

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            footerText: "",
            footerSignoff: "Thank you for your business!",
        };
    }
    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value,
        });
    };
    render() {
        const footerText = this.state.footerText.length > 0 ? this.state.footerText : `Make all checks payable to ${this.props.fromCompany}`;
        return (
            <div className="Footer">
                <div>
                    <textarea name="footerText" onChange={this.handleChange} value={footerText}></textarea>
                </div>
                <div>
                    <textarea className="footerSignoff" name="footerSignoff" onChange={this.handleChange} value={this.state.footerSignoff}></textarea>
                </div>
            </div>
        );
    }
}

export default Footer;
