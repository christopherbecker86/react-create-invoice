import React, { Component } from 'react';
import Item from './Item';

class Body extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [
                {
                    desc: "",
                    price: 0,
                }
            ],
        };
    }
    handleChange = (index, name, value) => {
        let items = this.state.items;
        items[index][name] = value;
        this.setState({
            items,
        });
    };
    addNewLine = () => {
        let items = this.state.items;
        items.push({
            desc: "",
            price: 0,
        });
        this.setState({
            items,
        });
    };
    render() {
        const itemsArray = this.state.items.map((item, index) => <Item handleChange={this.handleChange} item={item} index={index} />);
        const totalPrice = this.state.items.reduce((total, amount) => parseFloat(total) + parseFloat(amount.price), 0.00);
        return (
            <div className="Body">
                <div className="box">
                    <div className="col-8">
                        <strong>Description</strong>
                    </div>
                    <div className="col-2 text-right">
                        <strong>Amount</strong>
                    </div>
                </div>
                {itemsArray}
                <div className="hide-on-print">
                    <a href="#" onClick={this.addNewLine}>+ New line</a>
                </div>
                <div className="text-right">
                    <strong>TOTAL</strong> {totalPrice.toFixed(2)}
                </div>
              </div>
        );
    }
}

export default Body;
