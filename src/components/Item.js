import React, { Component } from 'react';

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            desc: "",
            price: 0,
        };
    }
    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.props.handleChange(this.props.index ,name, value);
    };
    render() {
        const { desc, price } = this.props.item;
        return (
            <div className="item box">
                <div className="col-8">
                    <input type="text" name="desc" onChange={this.handleChange} value={desc} />
                </div>
                <div className="col-2">
                    <input type="number" name="price" className="text-right" onChange={this.handleChange} value={price} />
                </div>
            </div>
        );
    }
}

export default Item;