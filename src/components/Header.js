import React, { Component } from 'react';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fromAddress: "",
            fromPhone: "",
            toName: "",
            toAddress: "",
            toPhone: "",
            date: "",
            invoiceNr: "",
        };
    }
    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value,
        });
    };
    handlePropChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.props.AppHandleChange(name, value);
    };
    render() {
        return (
            <div className="Header">
                <div className="box">
                    <div className="col-5">
                        <input class="fromCompany" name="fromCompany" type="text" value={this.props.fromCompany} onChange={this.handlePropChange} />
                    </div>
                    <div className="invoice-heading col-5">
                        INVOICE
                    </div>
                </div>
                <div className="box">
                    <div className="col-5">
                        <div>
                            <input name="fromAddress" placeholder="Street address" type="text" value={this.props.fromAddress} onChange={this.handleChange} />
                        </div>
                        <div>
                            <input name="fromPhone" placeholder="Phone number" type="text" value={this.props.fromPhone} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="col-5">
                        <div className="box">
                            <div className="col-5">
                                &nbsp;
                            </div>
                            <div className="col-5 text-right">
                                <strong>Date</strong>
                                <input name="date" className="text-right" type="text" value={this.props.date} onChange={this.handleChange} />
                            </div>
                        </div>
                        <div className="box">
                            <div className="col-5">
                                &nbsp;
                            </div>
                            <div className="col-5 text-right">
                                <strong>Invoice #</strong>
                                <input name="invoiceNr" className="text-right" type="text" value={this.props.invoiceNr} onChange={this.handleChange} />
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <strong>Bill To</strong>
                </div>
                <div className="box">
                    <div className="col-5">
                        <div>
                            <input name="toName" type="text" placeholder="Company name" value={this.props.toName} onChange={this.handleChange} />
                        </div>
                        <div>
                            <input name="toAddress" type="text" placeholder="Address" value={this.props.toAddress} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="col-5"></div>
                </div>
            </div>
        );
    }
}

export default Header;
